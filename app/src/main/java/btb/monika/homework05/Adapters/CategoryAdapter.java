package btb.monika.homework05.Adapters;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import btb.monika.homework05.Models.ListModel;
import btb.monika.homework05.Models.CategoryModel;
import btb.monika.homework05.R;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private ArrayList<CategoryModel> categoryModelArrayList;
    public Context cxt;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView category;
        public RecyclerView childRecyclerView;

        public MyViewHolder(View itemView) {
            super(itemView);

            category = itemView.findViewById(R.id.Movie_category);
            childRecyclerView = itemView.findViewById(R.id.Child_RV);
        }
    }

    public CategoryAdapter(ArrayList<CategoryModel> exampleList, Context context) {
        this.categoryModelArrayList = exampleList;
        this.cxt = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_activity, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return categoryModelArrayList.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        CategoryModel currentItem = categoryModelArrayList.get(position);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(cxt, LinearLayoutManager.HORIZONTAL, false);
        holder.childRecyclerView.setLayoutManager(layoutManager);
        holder.childRecyclerView.setHasFixedSize(true);

        holder.category.setText(currentItem.movieCategory());
        ArrayList<ListModel> arrayList = new ArrayList<>();

        // added the first child row
        if (categoryModelArrayList.get(position).movieCategory().equals("Trending Now")) {
            arrayList.add(new ListModel(R.drawable.d1, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d2, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d3, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d4, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d5, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d6, "Movie Name"));
        }

        // added in second child row
        if (categoryModelArrayList.get(position).movieCategory().equals("New Movies")) {
            arrayList.add(new ListModel(R.drawable.d7, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d8, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d9, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d10, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d11, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d12, "Movie Name"));
        }

        // added in third child row
        if (categoryModelArrayList.get(position).movieCategory().equals("You may like")) {
            arrayList.add(new ListModel(R.drawable.d13, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d14, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d15, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d16, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d17, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d1, "Movie Name"));
        }

        // added in fourth child row
        if (categoryModelArrayList.get(position).movieCategory().equals("TV Series")) {
            arrayList.add(new ListModel(R.drawable.d16, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d4, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d5, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d6, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d7, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d8, "Movie Name"));
        }

        ListMovieAdapter childRecyclerViewAdapter = new ListMovieAdapter(arrayList, holder.childRecyclerView.getContext());
        holder.childRecyclerView.setAdapter(childRecyclerViewAdapter);
    }
}