package btb.monika.homework05.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import btb.monika.homework05.Models.CategoryModel;
import btb.monika.homework05.Models.ListModel;
import btb.monika.homework05.R;

public class LargerCategoryAdapter extends RecyclerView.Adapter<LargerCategoryAdapter.MyViewHolder> {

    private ArrayList<CategoryModel> categoryModelArrayList;
    public Context cxt;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView largercategory;
        public RecyclerView largerRecyclerView;

        public MyViewHolder(View itemView) {
            super(itemView);

            largercategory = itemView.findViewById(R.id.Movie_category);
            largerRecyclerView = itemView.findViewById(R.id.Child_RV);
        }
    }

    public LargerCategoryAdapter(ArrayList<CategoryModel> exampleList, Context context) {
        this.categoryModelArrayList = exampleList;
        this.cxt = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.larger_category_activity, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return categoryModelArrayList.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        CategoryModel currentItem = categoryModelArrayList.get(position);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(cxt, LinearLayoutManager.HORIZONTAL, false);
        holder.largerRecyclerView.setLayoutManager(layoutManager);
        holder.largerRecyclerView.setHasFixedSize(true);

        holder.largercategory.setText(currentItem.movieCategory());
        ArrayList<ListModel> arrayList = new ArrayList<>();

        // added the first child row
        if (categoryModelArrayList.get(position).movieCategory().equals("Trending Now")) {
            arrayList.add(new ListModel(R.drawable.d1, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d2, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d3, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d4, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d5, "Movie Name"));
            arrayList.add(new ListModel(R.drawable.d6, "Movie Name"));
        }

        LargerListMovieAdapter largerRecyclerViewAdapter = new LargerListMovieAdapter(arrayList, holder.largerRecyclerView.getContext());
        holder.largerRecyclerView.setAdapter(largerRecyclerViewAdapter);
    }
}