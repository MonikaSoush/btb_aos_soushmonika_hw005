package btb.monika.homework05.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import btb.monika.homework05.Models.ListModel;
import btb.monika.homework05.R;

public class LargerListMovieAdapter extends RecyclerView.Adapter<LargerListMovieAdapter.MyViewHolder> {
    public ArrayList<ListModel> listModelArrayList;
    Context cxt;

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView largerImage;
        public TextView largerName;

        public MyViewHolder(View itemView) {
            super(itemView);
            largerImage = itemView.findViewById(R.id.hero_image);
            largerName = itemView.findViewById(R.id.movie_name);

        }
    }

    public LargerListMovieAdapter(ArrayList<ListModel> arrayList, Context mContext) {
        this.cxt = mContext;
        this.listModelArrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.larger_list_activity, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ListModel currentItem = listModelArrayList.get(position);
        holder.largerImage.setImageResource(currentItem.getHeroImage());
        holder.largerName.setText(currentItem.getMovieName());

    }

    @Override
    public int getItemCount() {
        return listModelArrayList.size();
    }
}