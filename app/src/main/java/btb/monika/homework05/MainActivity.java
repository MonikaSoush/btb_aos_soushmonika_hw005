package btb.monika.homework05;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

import btb.monika.homework05.Adapters.CategoryAdapter;
import btb.monika.homework05.Models.CategoryModel;

public class MainActivity extends AppCompatActivity {

    private RecyclerView parentRecyclerView;
    private RecyclerView.Adapter ParentAdapter;
    ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
    private RecyclerView.LayoutManager parentLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        //set the Categories for each array list set in the `ParentViewHolder`
        categoryModelArrayList.add(new CategoryModel("Trending Now"));
        categoryModelArrayList.add(new CategoryModel("New Movies"));
        categoryModelArrayList.add(new CategoryModel("You may like"));
        categoryModelArrayList.add(new CategoryModel("TV Series"));
        parentRecyclerView = findViewById(R.id.Parent_recyclerView);
        parentRecyclerView.setHasFixedSize(true);
        parentLayoutManager = new LinearLayoutManager(this);
        ParentAdapter = new CategoryAdapter(categoryModelArrayList, MainActivity.this);
        parentRecyclerView.setLayoutManager(parentLayoutManager);
        parentRecyclerView.setAdapter(ParentAdapter);
        ParentAdapter.notifyDataSetChanged();

    }

}