package btb.monika.homework05.Models;

public class CategoryModel {

    private String movieCategory;
    public CategoryModel(String movieCategory) {
        this.movieCategory = movieCategory;
    }
    public String movieCategory() {
        return movieCategory;
    }

}
