package btb.monika.homework05.Models;

public class ListModel {

    private  int hero_image;
    private String movieName;

    public ListModel(int hero_image, String movieName){
        this.hero_image = hero_image;
        this.movieName = movieName;
    }
    public int getHeroImage() {
        return hero_image;
    }
    public String getMovieName() {
        return movieName;
    }

}
